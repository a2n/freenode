package main

import (
    "log"
    "net"
    "bufio"
    "fmt"
    "crypto/tls"
    "runtime"
)

func main() {
    runtime.GOMAXPROCS(2^6)
    conn := connect()

    reader := make(chan string, 2^10)
    connected := make(chan bool)
    go backgroundReader(conn, reader);
    go func() {
	for {
	    str := <-reader
	    log.Print(str)
	    connected<-true
	}
    }()

    writer := make(chan string)
    result := make(chan bool)
    go func() {
	for {
	    <-connected
	    log.Println("connected")

	    go backgroundWriter(conn, writer, result)
	    writer<-"hello"
	    for {
		log.Printf("write result: %t\n", <-result)
	    }
	}
    }()

    defer conn.Close()
    log.Println("Connection closed.")
}

func backgroundReader(conn net.Conn, result chan string) {
    reader := bufio.NewReader(conn)
    for {
	str, _, err := reader.ReadLine()
	if err != nil {
	    log.Fatal(err)
	}
	result<-fmt.Sprintf("%s\n", str)
    }
}

func backgroundWriter(conn net.Conn, input chan string, result chan bool) {
    for {
	str := <-input
	log.Println(str)
	_, err := fmt.Fprintln(conn, str)
	if err != nil {
	    result<-false
	    log.Fatal(err)
	} else {
	    result<-true
	}
    }
}

func connect() (net.Conn) {
    // 6666 (non-ssl), 7000 (ssl)
    //conn, err := net.Dial("tcp", "irc.freenode.net:6666")
    conn, err := tls.Dial("tcp", "irc.freenode.net:7000", &tls.Config{})
    if err != nil {
	log.Fatal(err)
    }

    return conn
}
